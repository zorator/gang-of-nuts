import React from 'react';
import { BrowserRouter, Route, Switch } from "react-router-dom";
import styles from "./App.module.scss"
import Game from "./page/Game";
import Home from "./page/Home";
import Rules from "./page/Rules";
import Setup from "./page/Setup";

function App() {

	return (
		<BrowserRouter basename={process.env.PUBLIC_URL}>
		<div className={styles.container}>
			<Switch>
				<Route path="/game" component={Game}/>
				<Route path="/rules" component={Rules}/>
				<Route path="/setup" component={Setup}/>
				<Route path="/" component={Home}/>
			</Switch>
		</div>
		</BrowserRouter>
	);
}

export default App;
