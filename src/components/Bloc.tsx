import React, { FC } from 'react';

interface Props {
	className?: string
	heroClassName?: string
}

const Bloc: FC<Props> = ({
	                         className = 'column col-xs-12 col-auto',
	                         heroClassName,
	                         children
                         }) =>
	<div className={className}>
		<div className={["hero", heroClassName].join(' ')}>
			<div className="hero-body">
				{children}
			</div>
		</div>
	</div>

export default Bloc;
