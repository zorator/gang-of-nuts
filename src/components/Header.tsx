import React from 'react';
import { Link } from 'react-router-dom';

const Header = () => <header className="navbar">
	<section className="navbar-section">
		<Link to="/"><i className="icon icon-2x icon-arrow-left"/> Retour</Link>
	</section>
</header>

export default Header;
