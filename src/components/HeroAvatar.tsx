import React, { FC } from "react";
import { Hero } from "../domain/hero.model";
import styles from "./HeroAvatar.module.scss";

interface Props {
	hero: Hero
	size?: undefined | 'small'
}

const HeroAvatar: FC<Props> = ({hero, size}) =>
	<img className={[styles.avatar, size ? styles[size] : ''].join(' ')}
	     src={hero.avatar} alt={hero.name}/>

export default HeroAvatar
