import Header from "./Header";

const ComingSoon = () => <div>
	<Header/>
	<h1>Coming soon !!</h1>
</div>

export default ComingSoon
