import React, { FC, ReactNode } from "react";
import FormGroup from "./FormGroup";

interface Props {
	label: ReactNode
	tooltip: string
	name: string
	value: number
	onChange: (value: number) => void
	min?: number
	max?: number
}

const NumberInput: FC<Props> = ({tooltip, label, value, name, onChange, min=0, max}: Props) => {
	return <FormGroup label={label} tooltip={tooltip}>
		<input name={name} min={min} max={max}
		       value={value}
		       onChange={e => onChange(Number(e.target.value))}
		       className="form-input" type="number"/>
	</FormGroup>
}

export default NumberInput
