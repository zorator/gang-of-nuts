import { useEffect, useState } from 'react'
import RankConfig from "../../config/rank";
import { Config, ScoreConfig } from "../../domain/config.model";
import CountModel from "../../domain/count.model";

const calculateCount = (count: number, configs: Config[]): number => {
	// sort from min to max
	configs.sort((a, b) => a.from - b.from)
	let array = new Array(count || 0)
	for (let config of configs) {
		array.fill(config.points, config.from - 1)
	}
	return array.reduce((total, currentValue) => total + currentValue, 0);
}

const calculateScore = (counts: CountModel, scoreConfig: ScoreConfig, playerCount: number): number => {
	return calculateCount(counts.nutCount, scoreConfig.nutConfigs)
		+ calculateCount(counts.leafCount, scoreConfig.leafConfigs)
		+ calculateCount(counts.pineCount, scoreConfig.pineConfigs)
		+ counts.humanCount * scoreConfig.humanValue
		+ counts.lotusScore
		+ counts.unusedBonusCount * scoreConfig.unusedBonusValue
		+ counts.metHumanCount * scoreConfig.metHumanValue
		+ RankConfig.getRankBonus(playerCount, counts.rank)
		+ counts.questScore
}

const useTotalScore = (counts: CountModel, scoreConfig: ScoreConfig, playersCount: number): number => {

	const [totalScore, setTotalScore] = useState<number>(0)

	useEffect((): void => {
		if (counts != null) {
			setTotalScore(calculateScore(counts, scoreConfig, playersCount))
		}
	}, [counts, scoreConfig, playersCount])

	return totalScore
}

export default useTotalScore
