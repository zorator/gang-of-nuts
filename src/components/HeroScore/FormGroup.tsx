import React, { FC, PropsWithChildren, ReactNode } from "react";

interface Props {
	label: ReactNode
	tooltip: string
}

const FormGroup: FC<Props> = ({label, tooltip, children}: PropsWithChildren<Props>) => {
	return <div className="form-group" style={{alignItems: 'center'}}>
		<div className="column col-auto tooltip tooltip-custom-left" data-tooltip={tooltip}>
			<label className="form-label">{label}</label>
		</div>
		<div className="column">
			{children}
		</div>
	</div>
}

export default FormGroup
