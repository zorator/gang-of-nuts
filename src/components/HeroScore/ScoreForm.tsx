import React, { FC, useEffect, useState } from "react";
import CountModel from "../../domain/count.model";
import { Hero } from "../../domain/hero.model";
import bonusIcon from '../../style/png/resources/bonus.png'
import hazelnutIcon from '../../style/png/resources/hazelnut.png'
import humanGreenIcon from '../../style/png/resources/human-green.png'
import humanIcon from '../../style/png/resources/human.png'
import herbsIcon from '../../style/png/resources/leave.png'
import lotusIcon from '../../style/png/resources/lotus.png'
import pineIcon from '../../style/png/resources/pine.png'
import questIcon from '../../style/png/resources/quest.png'
import rankIcon from '../../style/png/resources/rank.png'
import HeroSelect from "./HeroSelect";
import NumberInput from "./NumberInput";
import styles from "./ScoreForm.module.scss"
import useTotalScore from "./useTotalScore.hook";

const initialCount: CountModel = {
	nutCount: 0, leafCount: 0, pineCount: 0, humanCount: 0, lotusScore: 0,
	unusedBonusCount: 0, metHumanCount: 0, rank: 1, questScore: 0
};

interface Props {
	heroes: Hero[]
	initialRank: number
	playersCount: number
	onChange: (hero: Hero, score: number) => void
}

const ScoreForm: FC<Props> = ({heroes, playersCount,initialRank, onChange}) => {

	const [hero, setHero] = useState<Hero>(heroes[0]);
	const [counts, setCounts] = useState<CountModel>({...initialCount, rank: initialRank});
	const totalScore = useTotalScore(counts, hero.scoreConfig, playersCount);

	const update = (key: keyof CountModel) => (value: any) => {
		setCounts({...counts, [key]: value})
	}

	const input = (key: keyof CountModel, img: string, tooltip: string, min?: number, max?: number) =>
		<NumberInput name={key} value={counts[key]} onChange={update(key)}
		             min={min} max={max} tooltip={tooltip}
		             label={<img className={styles.icon} src={img} alt={tooltip}/>}/>

	useEffect((): void => {
		onChange(hero, totalScore)
		// eslint-disable-next-line react-hooks/exhaustive-deps
	}, [hero, totalScore])

	return <form className={["form-horizontal", styles.form].join(' ')}>
		<div>
			<HeroSelect heroes={heroes} value={hero} onChange={setHero}/>
			<hr/>
			<h3>Nombre de tuiles</h3>
			{input("nutCount", hazelnutIcon, "Nombre de tuiles \"Noisette\" récoltées")}
			{input("leafCount", herbsIcon, "Nombre de tuiles \"Brindille\" récoltées")}
			{input("pineCount", pineIcon, "Nombre de tuiles \"Pomme de pin\" récoltées")}
			{input("unusedBonusCount", bonusIcon, "Nombre de bonus non-utilisés")}
			{hero.scoreConfig.metHumanValue !== 0
				? input("metHumanCount", humanIcon, "Nombre de tuiles malus \"Humain\" obtenus")
				: null}
			{hero.scoreConfig.humanValue !== 0
				? input("humanCount", humanGreenIcon, "Nombre de tuiles \"Humain\" récoltées")
				: null}
		</div>
		<div>
			<h3>Nombre de points</h3>
			{input("lotusScore", lotusIcon, "Nombre de points de victoire obtenus grace aux bonus")}
			{input("rank", rankIcon, "Position finale du joueur", 1, playersCount)}
			{input("questScore", questIcon, "Nombre de points de victoire obtenus avec les quêtes")}
		</div>
	</form>
}
export default ScoreForm
