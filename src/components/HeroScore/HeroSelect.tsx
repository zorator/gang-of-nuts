import React, { FC, useState } from "react";
import { Hero } from "../../domain/hero.model";
import HeroAvatar from "../HeroAvatar";
import styles from "./HeroSelect.module.scss"

interface Props {
	heroes: Hero[]
	value: Hero
	onChange: (hero: Hero) => void
}

const HeroSelect: FC<Props> = ({heroes, value, onChange}) => {

	const [index, setIndex] = useState<number>()

	const changeIndex = (pos: number): void => {
		let newIndex = ((index || 0) + pos + heroes.length) % heroes.length
		setIndex(newIndex)
		onChange(heroes[newIndex])
	}

	return <div>
		<div className={styles.heroName}>
			<h2 style={{color: value.color}}>{value.name}</h2>
		</div>
		<div className={styles.heroCarousel}>
			<i className={["icon icon-4x icon-caret", styles.caretLeft].join(' ')}
			   onClick={() => changeIndex(-1)}/>
			<HeroAvatar hero={value}/>
			<i className={["icon icon-4x icon-caret", styles.caretRight].join(' ')}
			   onClick={() => changeIndex(1)}/>
		</div>
	</div>
}

export default HeroSelect
