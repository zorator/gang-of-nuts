import React, { FC } from 'react';
import ScoreModel from "../domain/score.model";
import HeroAvatar from "./HeroAvatar";

import styles from './Podium.module.scss'

interface Props {
	scores: ScoreModel[]
}

const compareByScore = (a: ScoreModel, b: ScoreModel): number => b.score - a.score

const Podium: FC<Props> = ({
	                           scores
                           }) => {

	return <div className={styles.podium}>
		<h2>Podium</h2>
		{[...scores].sort(compareByScore).map(({hero, score}: ScoreModel, index: number) =>
			<div key={`${hero.id}-${index}`} className={styles.podiumRank}>
				<div className={styles.rank}>{index + 1}</div>
				<HeroAvatar hero={hero} size="small"/>
				<div className={styles.score}>{score}</div>
			</div>
		)}
	</div>;
}

export default Podium;
