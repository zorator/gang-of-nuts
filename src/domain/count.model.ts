interface CountModel {
	nutCount: number
	leafCount: number
	pineCount: number
	humanCount: number
	lotusScore: number
	unusedBonusCount: number
	metHumanCount: number
	rank: number
	questScore: number
}

export default CountModel
