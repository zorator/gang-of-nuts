export interface Config {
	from: number
	points: number
}

export interface ScoreConfig {
	nutConfigs: Config[]
	leafConfigs: Config[]
	pineConfigs: Config[]
	humanValue: number
	unusedBonusValue: number
	metHumanValue: number
}
