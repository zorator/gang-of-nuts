import { Hero } from "./hero.model";

interface ScoreModel {
	hero: Hero
	score: number
}

export default ScoreModel
