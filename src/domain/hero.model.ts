import { ScoreConfig } from "./config.model";

export interface Hero {
	id: number
	name: string
	title: string
	avatar: string
	color: string
	scoreConfig: ScoreConfig
}
