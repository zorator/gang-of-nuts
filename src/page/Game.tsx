import React, { useState } from 'react';
import Bloc from "../components/Bloc";
import Header from "../components/Header";
import ScoreForm from "../components/HeroScore/ScoreForm";
import Podium from "../components/Podium";
import heroList from "../config/heros";
import { MAX_PLAYERS, MIN_PLAYERS } from "../config/rank";
import { Hero } from "../domain/hero.model";
import ScoreModel from "../domain/score.model";
import styles from './Game.module.scss'

const getAvailableHeroes = (scores: ScoreModel[], playerIndex: number): Hero[] => {
	let selectedIds: number[] = scores
	.filter((score: ScoreModel, index: number) => index !== playerIndex)
	.map((score: ScoreModel) => score.hero.id)
	return heroList.filter((hero: Hero) => !selectedIds.includes(hero.id))
}

function Game() {

	const [scores, setScores] = useState<ScoreModel[]>([
		{hero: heroList[0], score: 0},
		{hero: heroList[1], score: 0}
	])
	const [playerCount, setPlayersCount] = useState<number>(MIN_PLAYERS)

	const updateIndex = (index: number) => (hero: Hero, score: number): void => {
		scores[index] = {hero, score}
		setScores([...scores])
	}

	const removeHero = (index: number) => {
		scores.splice(index, 1)
		setScores([...scores])
		setPlayersCount(playerCount - 1)
	}

	return <>
		<Header/>
		<div className="columns justify-content-center">
			{Array.from(Array(playerCount).keys()).map((index: number) =>
				<Bloc key={index} heroClassName={styles.blocHeroScore}>
					{index > 1 ? <button className={["btn btn-action s-circle tooltip", styles.removeBtn].join(' ')}
					                     data-tooltip="Retirer ce joueur" onClick={() => removeHero(index)}>
						<i className="icon icon-cross"/>
					</button> : null}
					<ScoreForm heroes={getAvailableHeroes(scores, index)}
					           playersCount={playerCount} initialRank={index + 1}
					           onChange={updateIndex(index)}/>
				</Bloc>
			)}

			{playerCount < MAX_PLAYERS
				? <Bloc heroClassName="text-center">
					<button className="btn btn-primary" onClick={() => setPlayersCount(playerCount + 1)}>
						<i className="icon icon-plus"/> Ajouter un joueur
					</button>
				</Bloc>
				: null}
		</div>
		<div className="columns justify-content-center">
			<Bloc>
				<Podium scores={scores}/>
			</Bloc>
		</div>

	</>
}

export default Game;
