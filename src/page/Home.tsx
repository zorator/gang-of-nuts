import React, { FC, ReactNode } from "react";
import { Link } from "react-router-dom";
import hazelnutIcon from '../style/png/resources/hazelnut.png'
import herbsIcon from '../style/png/resources/leave.png'
import pineIcon from '../style/png/resources/pine.png'

import styles from './Home.module.scss'

interface HomeLinkProp {
	to: string
	img: ReactNode
	colorClassName: string
}

const HomeLink: FC<HomeLinkProp> = ({to, img, colorClassName, children: title}) =>
	<Link to={to} className={[styles.HomeLink, colorClassName].join(' ')}>
		<div className={styles.HomeLinkImg}>{img}</div>
		<h2 className={styles.HomeLinkTitle}>{title}</h2>
	</Link>

const Home: FC = () => <div className={styles.Home}>
	<h1>Gang Of Nuts</h1>
	<nav className={styles.HomeNav}>
		<HomeLink to="/game"
	               colorClassName={styles.colorGreen}
	               img={<img src={hazelnutIcon} alt="Noisettes"/>}>
		Nouvelle partie
	</HomeLink>
		<HomeLink to="/rules"
		          colorClassName={styles.colorRed}
		          img={<img src={herbsIcon} alt="Brindille"/>}>
			Règles
		</HomeLink>
		<HomeLink to="/setup"
		          colorClassName={styles.colorYellow}
		          img={<img src={pineIcon} alt="Pomme de pin"/>}>
			Mise en place
		</HomeLink>
	</nav>
</div>

export default Home
