import { Hero } from "../domain/hero.model";

import bibouIco from "../style/png/heroes/bibou.png";
import casssieIco from "../style/png/heroes/casssie.png";
import neuneuilleIco from "../style/png/heroes/neuneuille.png";
import rogerIco from "../style/png/heroes/roger.png";
import titiIco from "../style/png/heroes/titi.png";
import woodyIco from "../style/png/heroes/woody.png";
import defaultConfig from "./score";

const heroList: Hero[] = [{
	id: 1,
	name: 'Roger',
	title: 'Lapin pressé',
	avatar: rogerIco,
	color: '#AE5743',
	scoreConfig: defaultConfig
}, {
	id: 2,
	name: 'CasSsie',
	title: 'Reptile habile',
	avatar: casssieIco,
	color: '#d99f37',
	scoreConfig: {...defaultConfig, humanValue: 3}
}, {
	id: 3,
	name: 'Neuneuille',
	title: 'Écureuil glandeur',
	avatar: neuneuilleIco,
	color: '#8acb7f',
	scoreConfig: defaultConfig
}, {
	id: 4,
	name: 'Bibou',
	title: 'Volatile plutôt chouette',
	avatar: bibouIco,
	color: '#AE5743',
	scoreConfig: {...defaultConfig, metHumanValue: 0}
}, {
	id: 5,
	name: 'Titi',
	title: 'Termite audacieuse',
	avatar: titiIco,
	color: '#AE5743',
	scoreConfig: defaultConfig
}, {
	id: 6,
	name: 'Woody',
	title: 'Pic-vert téméraire',
	avatar: woodyIco,
	color: '#AE5743',
	scoreConfig: {
		...defaultConfig,
		nutConfigs: [{from: 1, points: 4}, {from: 11, points: 5}, {from: 18, points: 6}]
	}
}]

export default heroList
