export interface RankByPlayersConfig {
	[a: number]: number[]
}

const rankByPlayersConfig: RankByPlayersConfig = {
	2: [3, 0],
	3: [4, 2, 0],
	4: [5, 3, 1, 0]
}

export const MIN_PLAYERS: number = 2
export const MAX_PLAYERS: number = 4

class RankConfig {
	static getRankBonus = (playerCount: number, finalRank: number): number => {
		if (playerCount < MIN_PLAYERS || playerCount > MAX_PLAYERS
			|| finalRank < 1 || finalRank > playerCount) {
			return 0;
		}
		return rankByPlayersConfig[playerCount][finalRank - 1]
	}
}

export default RankConfig
