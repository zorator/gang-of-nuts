import { ScoreConfig } from "../domain/config.model";

const defaultConfig: ScoreConfig = {
	nutConfigs: [{from: 1, points: 1}, {from: 9, points: 2}, {from: 17, points: 3}],
	leafConfigs: [{from: 1, points: 2}, {from: 6, points: 3}, {from: 9, points: 4}],
	pineConfigs: [{from: 1, points: 3}, {from: 4, points: 5}, {from: 6, points: 6}],
	humanValue: 0,
	unusedBonusValue: 2,
	metHumanValue: -3,
}

export default defaultConfig
